/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorizer;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Factorizer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numberToBeFactored, sumOfAllFactors = 0;
        int numberOfFactors = 0;
        Scanner scan = new Scanner(System.in);
        numberToBeFactored = getFactorFromUser(scan);
        sumOfAllFactors = listOfTotalFactors(numberToBeFactored, numberOfFactors, sumOfAllFactors);
        printPerfectNumber(sumOfAllFactors, numberToBeFactored);
        printPrimeNumber(numberToBeFactored);
    }

    public static int getFactorFromUser(Scanner scan) {
        int numberToBeFactored;
        System.out.print("What number do you want factored? ");
        numberToBeFactored = scan.nextInt();
        return numberToBeFactored;
    }

    public static void printPrimeNumber(int numberToBeFactored) {
        if (isPrime(numberToBeFactored)) {
            System.out.println(numberToBeFactored + " is a prime number");
        } else {
            System.out.println(numberToBeFactored + " is not a prime number");
        }
    }

    public static void printPerfectNumber(int sumOfAllFactors, int numberToBeFactored) {
        if (sumOfAllFactors == numberToBeFactored) {
            System.out.println(numberToBeFactored + " is a perfect number.");
        } else {
            System.out.println(numberToBeFactored + " is not a perfect number");
        }
    }

    public static int listOfTotalFactors(int numberToBeFactored, int numberOfFactors, int sumOfAllFactors) {
        for (int i = 1; i <= numberToBeFactored; i++) {
            if (numberToBeFactored % i == 0) {
                System.out.println(i);
                ++numberOfFactors;
            }
            if (numberToBeFactored % i == 0 && i < numberToBeFactored) {
                sumOfAllFactors = sumOfAllFactors + i;
            }
        }
        System.out.println(numberToBeFactored + " has " + numberOfFactors + " factors");
        return sumOfAllFactors;
    }
    public static boolean isPrime(int numberToBeFactored) {
        if (numberToBeFactored <= 1) {
            return false;
        }
        for (int i = 2; i < Math.sqrt(numberToBeFactored); i++) {
            if (numberToBeFactored % i == 0) {
                return false;
            }
        }
        return true;
    }
}
