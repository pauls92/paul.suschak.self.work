/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interest.calculator;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculator {
    
    // TODO : Break code in Main method down to other smaller methods
    // 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        calculateInterest(input);    
    }

    public static void calculateInterest(Scanner input) {
        double initialPrinciple = getUserPrinciple(input);
        int yrs = getUserTotalYears(input);
        double annualInterestRate = getUserInterestRate(input);
        calculateTotalInterestForYears(yrs, initialPrinciple, annualInterestRate);    
    }

    public static void calculateTotalInterestForYears(int yrs, double initialPrinciple, double annualInterestRate) {
        // System.out.print("The annual interest rate is " + rate + "%");
        System.out.println("Total amount compounded over the " + yrs + " years: ");

        double lastYearsBalance = initialPrinciple;

        // Make the below loop its own method
        
        for (int currentYear = 1; currentYear <= yrs; currentYear++) {

            double currentTotalAnnualAmount = calculateAnnualInterest(initialPrinciple, annualInterestRate, currentYear);
            double interestEarnedThisYear = currentTotalAnnualAmount - lastYearsBalance;

            System.out.printf("Year " + currentYear + ": %.2f, you earned %.2f interest this year, after having started with %.2f\n", currentTotalAnnualAmount, interestEarnedThisYear, lastYearsBalance);

            lastYearsBalance = currentTotalAnnualAmount;
        }
    }

    private static double getUserInterestRate(Scanner input) {
        double interestRate = 0;
        System.out.print("Enter Interest Rate: ");
        while (interestRate == 0) {
            try {
                interestRate = input.nextDouble();
            } catch (InputMismatchException|Error ime) {
                System.out.println("Invalid value, please enter a valid interest rate!");
                input.nextLine();                     
            }
        }
        return interestRate;
    }

    private static int getUserTotalYears(Scanner input) {
        System.out.print("Total number of years: ");
        int yrs = 0;
        while (yrs == 0) {
            try {
                yrs = input.nextInt();
            } catch (InputMismatchException ime) {
                System.out.println("Invalid number of years, please enter a valid number of years!");
                input.nextLine();
            }
        }
        return yrs;
    }

    private static double getUserPrinciple(Scanner input) {
        double userPrinciple = 0;
        System.out.print("Initial amount of principle: ");
        while (userPrinciple == 0) {
            try {
                userPrinciple = input.nextDouble();
            } catch (InputMismatchException ime) {
                System.out.println("Invalid priciple, please enter a valid amount!");
                input.nextLine();
            }
        }
        return userPrinciple;
    }

    public static double calculateAnnualInterest(double amt, double rate, int yrs) {
        return (amt * Math.pow(1 + rate, yrs));
    }

}
