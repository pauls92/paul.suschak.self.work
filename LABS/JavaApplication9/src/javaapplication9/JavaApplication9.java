/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication9;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class JavaApplication9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numberToBeFactored, sumOfAllFactors = 0;
        int numberOfFactors = 0;
        Scanner scan = new Scanner(System.in);
        System.out.print("What number do you want factored? ");
        numberToBeFactored = scan.nextInt();
        for (int i = 1; i <= numberToBeFactored; i++) {
            if (numberToBeFactored % i == 0) {
                System.out.println(i);
                ++numberOfFactors;
            }
            if (numberToBeFactored % i == 0 && i < numberToBeFactored) {
                sumOfAllFactors = sumOfAllFactors + i;
            }
        }
        System.out.println(numberToBeFactored + " has " + numberOfFactors + " factors");
        if (sumOfAllFactors == numberToBeFactored) {
            System.out.println(numberToBeFactored + " is a perfect number.");
        } else {
            System.out.println(numberToBeFactored + " is not a perfect number");
        }
    }
}
