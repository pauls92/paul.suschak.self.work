/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner bet = new Scanner(System.in);
        Random gameDie = new Random();

        int dieOne, dieTwo,
                playerBet,
                rollCountAtMaxDollarsWon,
                countTotalAtGameEnd,
                maxDollarsWon,
                initialDollars;

        playerBet = promptForUserBet(bet);

        maxDollarsWon = initializeVariables(playerBet);
        rollCountAtMaxDollarsWon = 0;
        countTotalAtGameEnd = 0;
        // loop until money is gone
        while (playerBet > 0) {
            countTotalAtGameEnd++;

            dieOne = gameDie.nextInt(6) + 1;
            dieTwo = gameDie.nextInt(6) + 1;

            if (dieOne + dieTwo == 7) {
                playerBet += 4;
            } else {
                playerBet -= 1;
            }
            if (playerBet > maxDollarsWon) {

                maxDollarsWon = playerBet;
                
                rollCountAtMaxDollarsWon = countTotalAtGameEnd;
            }
        }
        printGameStats(countTotalAtGameEnd, rollCountAtMaxDollarsWon, maxDollarsWon);
    }

    public static int promptForUserBet(Scanner bet) {
        int playerBet;
        System.out.println("How much money do you have to play?");
        playerBet = bet.nextInt();
        return playerBet;
    }

    public static int initializeVariables(int playerBet) {
        int maxDollarsWon;
        int initialDollars;
        // Initialize variables
        maxDollarsWon = playerBet;
        initialDollars = playerBet;
        return maxDollarsWon;
    }

    public static void printGameStats(int countTotalAtGameEnd, int rollCountAtMaxDollarsWon, int maxDollarsWon) {
        System.out.println("You went broke after " + countTotalAtGameEnd + " rolls.\n" + "You should've quit after "
                + rollCountAtMaxDollarsWon + " rolls when you had $" + maxDollarsWon + ".");
    }

}
