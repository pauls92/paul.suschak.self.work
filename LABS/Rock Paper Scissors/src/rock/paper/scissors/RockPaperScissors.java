/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rock.paper.scissors;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RockPaperScissors {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);

        System.out.print("Select rock, paper, or scissors.");
        String choice = scan.nextLine();
        
        if (isValid(choice)) {
            int computerPlay = (int)(Math.random()*3);
            int userPlay = getVal(choice);
            System.out.println("The computer picked " + getComputerPlay(computerPlay));
            System.out.println("You " + didPersonWin(userPlay, computerPlay));
        }
        else {
            System.out.println("This is not a valid input!");
        }
    }
    public static String getComputerPlay(int x) {
        if (x == 0) {
            return "rock";
        }
        if (x == 1) {
            return "paper";
        }
        if (x == 2) {
            return "scissors";
        }
        return "IDK";
    }
    public static String didPersonWin(int uPlay, int compPlay) {
        if (uPlay == 0) {
            if (compPlay != 1) {
                if (compPlay != 0) {
                    return "Win!";
                }
                return "Tie!";
            }
            return "Lose!";
        }
        if (uPlay == 1) {
            if (compPlay != 0) {
                if (compPlay != 1) {
                    return "Lose!";
                }
                return "Tie!";
            }
            return "Win!";           
        }
        if (uPlay == 2) {
            if (compPlay != 0) {
                if (compPlay != 2) {
                    return "Win!";
                }
                return "Tie";
            }
            return "Lose";
        }
        return "IDK";
    }

    public static boolean isValid(String string) {
        if (string.equalsIgnoreCase("rock")) {
            return true;
        }
        if (string.equalsIgnoreCase("paper")) {
            return true;      
        }
        if (string.equalsIgnoreCase("scissors")) {
            return true;
        }
        return false;
    }
    public static int getVal(String string) {
        if (string.equalsIgnoreCase("rock")) {
            return 0;
        }
        if (string.equalsIgnoreCase("paper")) {
            return 1;
        }
        else {
            return 2;
        }
    }
}
