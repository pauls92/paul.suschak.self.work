/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rockpaperscissorsstep1;

import java.util.Scanner;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class RockPaperScissorsStep1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String userPlay = promptForUserPlay();
        String computerPlay = generateComputerPlay();
        printPlays(userPlay, computerPlay);
        String outcome = determineOutcome(userPlay, computerPlay);
        displayOutcome(outcome);

    }

    private static String promptForUserPlay() {
        String play = "";
        Scanner scan = new Scanner(System.in);

        System.out.println("This is Rock Paper Scissors!");
        String any = scan.nextLine();

        System.out.println("What'll it be? (rock, paper, or scissors) ");
        play = scan.nextLine();

        if ("rock".equals(play) || "paper".equals(play) || "scissors".equals(play)) {
            System.out.println("You chose " + play);
        }
        return play;

    }

    private static String generateComputerPlay() {
        String[] arr = {"rock", "paper", "scissors"};
        Random random = new Random();
        int select = random.nextInt(arr.length);
        return arr[select];
    }

    private static void printPlays(String userPlay, String computerPlay) {
        System.out.println(String.format("User Picks: %1s - Computer Picks: %2s", userPlay, computerPlay));
    }

    private static String determineOutcome(String userPlay, String computerPlay) {
      
        
        
//       String r = "rock", p = "paper", s = "scissors";
//       if (userPlay.equals(r)) {
//           if (computerPlay.equals(p)){
//         return "Computer Wins!";
//       }
    return "It's a tie!";
    
 
    }

    private static void displayOutcome(String outcome) {
        System.out.println(String.format("Outcome: %1s", outcome));
    }

}
