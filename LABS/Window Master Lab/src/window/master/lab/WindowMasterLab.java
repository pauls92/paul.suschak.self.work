/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.master.lab;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMasterLab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner calc = new Scanner(System.in);

        System.out.print("What is the width of the window? ");
        double width = calc.nextDouble();

        System.out.print("What is the height of the window? ");
        double height = calc.nextDouble();

        double width2 = width * 2;
        double height2 = height * 2;

        double per = width2 + height2;
        System.out.println("Perimeter: " + per);

        double area = width * height;
        System.out.println("Area: " + area);

        double trim = per * 3;
        double glass = area * 2.5;

        double total = trim + glass;

        System.out.println("Window cost: " + glass);
        System.out.println("Trim cost: " + trim);
        System.out.println("");
        System.out.println("Total cost: " + total);
    }

}

}
