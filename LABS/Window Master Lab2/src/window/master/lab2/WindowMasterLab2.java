/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.master.lab2;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMasterLab2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner calc = new Scanner(System.in);

        double width = checkWidth(calc);
        double height = checkHeight(calc);            
        double per = calculatePerimeter(width, height);
        double area = calculateArea(width, height);     
        double glass = costOfGlass(area);       
        double trim = costOfTrim(per);
               
        totalCost(calc, trim, glass);
    }   

    private static double costOfTrim(double per) {
        double trim = per * 3;
        System.out.println("Trim cost: " + trim);
        return trim;
    }

    private static double costOfGlass(double area) {
        double glass = area * 2.5;
        System.out.println("Window cost: " + glass);
        return glass;
    }

    private static void totalCost(Scanner calc, double trim, double glass) {
        System.out.print("How many windows do you want? ");
        
        double num = calc.nextDouble();
        double trim2 = trim * num;
        double glass2 = glass * num;

        System.out.println("Cost of trim: " + trim2);
        System.out.println("Cost of glass: " + glass2);

        double material = trim2 + glass2;
        System.out.println("Final material cost: " + material);
    }   

    private static double calculateArea(double width, double height) {
        double area = width * height;
        System.out.println("Area: " + area);
        return area;
    }

    private static double calculatePerimeter(double width, double height) {
        double width2 = width * 2;
        double height2 = height * 2;
        double per = width2 + height2;
        System.out.println("Perimeter: " + per);
        return per;
    }

    private static double checkWidth(Scanner calc) {
        double width = 0;
        do {
            System.out.print("What is the width of the window? ");
            width = calc.nextDouble();
            if (width > 18.75) {
                System.out.println("Please enter a value 18.75 or lower.");
            } else if (width < 1) {
                System.out.println("Please enter a value 1 or higher.");
            }
        } while (width > 18.75 || width < 1);
        return width;
    }

    private static double checkHeight(Scanner calc) {
        double height = 0;
        do {
            System.out.print("What is the height of the window? ");
            height = calc.nextDouble();
            if (height > 25.5) {
                System.out.println("Please enter a value 25.5 or lower.");
            } else if (height < 1) {
                System.out.println("Please enter a value 1 or greater.");
            }
        } while (height > 25.5 || height < 1);
        if (height > 25.5) {
            System.out.println("Please re-enter a lower value");
        } else if (height < 1) {
            System.out.println("Please re-enter a greater value");
        }
        return height;
    }

}
