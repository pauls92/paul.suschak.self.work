/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package age.messages.pkg3;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgeMessages3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        
           System.out.print("Hey what's your name? ");
        String name = scan.nextLine();
        
        System.out.print("Ok, " + name + ", how old are you? ");
        int age = scan.nextByte();
        
        if (age < 16) {
            System.out.println("You can't drive.");
        } else if (age < 18) {
            System.out.println("You can't vote.");
        } else if (age < 25) {
            System.out.println("You can't rent a car.");
        } else if (age >= 25) {
            System.out.println("You can do pretty much anything.");
        }
    }
    
}
