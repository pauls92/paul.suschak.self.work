/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asking.questions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */

public class AskingQuestions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        int age;
        int height;
        int inches;
        double weight;
        
        System.out.println("How old are you? " );
        age = keyboard.nextInt();
        
        System.out.println("How many feet tall are you? " );
        height = keyboard.nextInt();
        
        System.out.println("And how many inches? " );
        inches = keyboard.nextInt();
        
        
        System.out.println("How much do you weigh? " );
        weight = keyboard.nextDouble();
        
        System.out.println("So you're " + age + " old, " + height + " " + inches + " tall and " + weight + " heavy." );
    }
    
}
