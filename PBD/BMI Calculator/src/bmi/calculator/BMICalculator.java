/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmi.calculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BMICalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner now = new Scanner(System.in);
        System.out.print("Your height in m: ");
        double height = now.nextDouble();
        
        System.out.print("Your weight in kg: ");
        double weight = now.nextDouble();
        System.out.println();
        
        double tower = height * height;
        
        double bmi = weight / tower;
        System.out.print("Your BMI is " + bmi);
        
    }
    
}
