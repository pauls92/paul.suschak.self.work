/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgboolean.expressions;

/**
 *
 * @author apprentice
 */
public class BooleanExpressions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // boolean playAgain = false;
//
//        playAgain = (1 == 2);
//
//        playAgain = (1 != 2);
//
//        playAgain = (true != false);
//
//        playAgain = (2 > 3);
//
//        playAgain = !playAgain;
//
//        if (!playAgain) {
//
//        }
//
//        if (1 > 2 && 2 > 3) {
//
//        }
//            if (1 > 2 || 3 > 2) {
//            
//        }
//        if (true ^ false) {
//
//        }
//        if (playAgain) {
//            System.out.println("We want to play again.");
//        } else {
//            System.out.println("We do not want to play again.");
//        }
//        if (playAgain) {
//            System.out.println("Play again is true");
//        } else if (1 > 2) {
//            System.out.println("1 greater than 2");
//        } else if (3 > 2) {
//            System.out.println("3 is greater than 2");
//        } else {
//            System.out.println("Neither one of those is true");
//        }
//        
//        int day = 3;
//        
//        if (day == 1) {
//            System.out.println("Today is monday");
//        } else if (day == 2) {
//            System.out.println("Today is tuesday");
//        } else if (day == 3) {
//            System.out.println("Today is wednesday");
//        } else if (day == 4) {
//            System.out.println("Today is thrusday");
//        } else if (day == 5) {
//            System.out.println("Today is friday");
//        } else if (day == 6) {
//            System.out.println("Today is saturday");
//        } else if (day == 7) {
//            System.out.println("Today is sunday");
//        }
//        
//        switch (day) {
//            case 1:
//                System.out.println("Today is monday");
//                break;
//            case 2:
//                System.out.println("Today is tuesday");
//                break;
//            case 3:
//                System.out.println("Today is wednesday");
//                break;
//            default:
//                System.out.println("Not a valid day");
//        }
//        switch (day) {
//            case 1:
//            case 2:
//            case 3:
//            case 4:
//            case 5:
//                System.out.println("Today is a weekday");
//                break;
//            case 6:
//            case 7:
//                System.out.println("Today is a weekend");
//            default:
//                System.out.println("Invalid day");
//        }
//        final int MAX_HEIGHT = 160; // THIS IS A CONSTANT
        
          boolean playAgain = true;

        do {
            System.out.println("In my do while loop");

            if (2 > 1) {

                playAgain = false;
            }

        } while (playAgain);

        playAgain = true;
        while (playAgain) {
            System.out.println("In my while loop");

            if (2 > 1) {
                playAgain = false;
            }
        }
        
        for (int i = 0; i < 10; i++) {
            System.out.println("In my for loop: " + i);
        }
    }
}
