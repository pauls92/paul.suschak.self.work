/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgboolean.expressions;

/**
 *
 * @author apprentice
 */
public class Loops {

    public static void main(String[] args) {

        boolean playAgain = true;

        do {
            System.out.println("In my do while loop");

            if (2 > 1) {

                playAgain = false;
            }

        } while (playAgain);

        playAgain = true;
        while (playAgain) {
            System.out.println("In my while loop");

            if (2 > 1) {
                playAgain = false;
            }
        }
        
        for (int i = 0; i < 10; i++) {
            System.out.println("In my for loop: " + i);
        }

    }

}
