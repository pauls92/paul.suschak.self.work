/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package choose.your.own.adventure;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ChooseYourOwnAdventure {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner room = new Scanner(System.in);
        
        String room1 = "";
        String room2 = "";
        String room3 = "";
        String room4 = "";
        String room5 = "";
        String room6 = "";
        String room7 = "";
              
        System.out.println("You are in a creepy house! Would you like to go upstairs or into the kitchen? ");
        room1 = room.nextLine();
        
        // the bottom question is if kitchen was answered
        System.out.println("There is a long countertop with dirty dishes everywhere. Off to one side there is, as you'd expect, a refridgerator. You may open the refridgerator, or open the cabinet.");
        room2 = room.nextLine();
        
        // the bottom question is for the upstairs answer
        System.out.println("Upstairs you see a hallway. At the end of the hallway is the master bedroom. There is also a bathroom off the hallway. Where would you like to go?");
        room5 = room.nextLine();
        // this is for the kitchen answer
        System.out.println("Inside the refridgerator you see food and stuff. It looks pretty nasty. Would you like to eat some of the food. (yes or no)");
        room3 = room.nextLine();
        
        System.out.println("Inside the cabinet is some cereal. Would you like some?");
        room4 = room.nextLine();
        
        // this is another upstairs question
        System.out.println("You are in a plush bedroom, with expensive-looking hardwood furniture. The bed is unmade. In the back of the room, the closet door is ajar.  Would you like to open the door? (yes or no)");
        room6 = room.nextLine();
        
        System.out.println("You're in the bathroom. There is a sound coming from behind the shower curtains, and you're the only one in the house. Do you want to investigate?");
        room7 = room.nextLine();
        
        if ("kitchen".equals(room1)) {
            if ("refridgerator".equals(room2)) {
                if ("yes".equals(room3)) {
                    System.out.println("Bon Appetite!");
                }
                    else {
                            System.out.println("Fine, starve then!");
                            }
                }
            if ("cabinet".equals(room2)) {
                if ("yes".equals(room4)) {
                    System.out.println("Here, have some creepy-o's!");
                }
                else {
                    System.out.println("You no like creepy-o's?!");
                }
            }
            }
        if ("upstairs".equals(room1)) {
            if ("bedroom".equals(room5)) {
                if ("yes".equals(room6)) {
                    System.out.println("Watch out for the boogeyman!");
                }
                else {
                    System.out.println("Wise decision.");
                }
            }
            if ("bathroom".equals(room5)) {
                if ("yes".equals(room7)) {
                    System.out.println("It's a ghost HOLY S@#T!");
                }
                else {
                    System.out.println("Get out of there while you still can!");
                }
            }
        }
        }
}
