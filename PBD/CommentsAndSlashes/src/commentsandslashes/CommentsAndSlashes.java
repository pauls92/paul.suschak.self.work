/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentsandslashes;

/**
 *
 * @author apprentice
 */
// my name is paul and today is august 1, 2016
// yes, this is the correct way to leave a comment
public class CommentsAndSlashes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("I could have code look like this."); // and the comment after is ignored
        // TODO code application logic here
        // you can also use // to "disable" or comment out a piece of code
        System.out.println("This will run");
    }
    
}
