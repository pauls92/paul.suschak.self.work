/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgelse.and.pkgif;

/**
 *
 * @author apprentice
 */
public class ElseAndIf {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int people = 30;
        int cars = 40;
        int buses = 15;

        if (cars > people) {
            System.out.println("We should take the cars.");
        } if (cars < people) {
            System.out.println("We should not take the cars.");
        } else {
            System.out.println("We can't decide.");
        }
        /* Else and else if statements provide multiple conditions within the if statement 
        through which variable/s pass */
        
        /* Removing else from an else if statement creates a new condition that
        is read without consideration of the preceding condition */
        if (buses > cars) {
            System.out.println("That's too many buses.");
        } else if (buses < cars) {
            System.out.println("Maybe we could take the buses.");
        } else {
            System.out.println("We still can't decide.");
        }

        if (people > buses) {
            System.out.println("All right, let's just take the buses.");
        } else {
            System.out.println("Fine, let's stay home then.");
        }
    }

}
