/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enter.your.age;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class EnterYourAge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter your age: ");
        int age = sc.nextByte();
        
        if (age < 18) {
            System.out.print("You must be in school");
        } else if (age > 18) {
            System.out.println("Time to go to work");
        } else if (age < 65) {
            System.out.println("Time to go to work");
        } else if (age > 65) {
            System.out.println("Enjoy your retirement");
        }
    }
}
