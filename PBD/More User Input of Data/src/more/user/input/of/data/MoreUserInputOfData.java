/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package more.user.input.of.data;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MoreUserInputOfData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner print = new Scanner(System.in);
        System.out.print("First Name:");
        String name = print.nextLine();
        System.out.print("Last Name: ");
        String last = print.nextLine();
        System.out.print("Grade (9-12):");
        int grade = print.nextByte();
        System.out.print("Student ID:");
        int id = print.nextByte();
        System.out.print("Login:");
        String log = print.nextLine();
        System.out.print("GPA (0.0-4.0:");
        double avg = print.nextDouble();

        
        System.out.println("Your Information:");
        System.out.println("Login: " + log);
        System.out.println("ID: " + id);
        System.out.println("Name: " + name + last);
        System.out.println("GPA: " + avg);
        System.out.println("Grade: " + grade);
    }
    
}
