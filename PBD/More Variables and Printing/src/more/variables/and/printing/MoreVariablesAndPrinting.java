/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package more.variables.and.printing;

/**
 *
 * @author apprentice
 */
public class MoreVariablesAndPrinting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        String Name = "Zed A. Shaw";
        int Age = 35;    
        int Height = 74;  // inches
        double Centi = Height * 2.54;
        int Weight = 180; // lbs
        double Kilo = Weight / 2.2;
        String Eyes = "Blue";
        String Teeth = "White";
        String Hair = "Brown";

        System.out.println( "Let's talk about " + Name + "." );
        System.out.println( "He's " + Centi + " centimeters tall." );
        System.out.println( "He's " + Kilo + " kilograms heavy." );
        System.out.println( "Actually, that's not too heavy." );
        System.out.println( "He's got " + Eyes + " eyes and " + Hair + " hair." );
        System.out.println( "His teeth are usually " + Teeth + " depending on the coffee." );

        // This line is tricky; try to get it exactly right.
        System.out.println( "If I add " + Age + ", " + Height + ", and " + Weight
            + " I get " + (Age + Height + Weight) + "." );
        // TODO code application logic here
    }
    
}
