/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package space.boxing;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class SpaceBoxing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);

        System.out.print("Please enter your current earth weight: ");
        double earth = scan.nextDouble();

        double venus = earth * 0.78;
        double mars = earth * 0.39;
        double jupiter = earth * 2.65;
        double saturn = earth * 1.17;
        double uranus = earth * 1.05;
        double neptune = earth * 1.23;

        System.out.println("");
        
        System.out.println("I have information for the following planets:");
        System.out.println("1. Venus     2. Mars      3. Jupiter");
        System.out.println("4. Saturn    5. Uranus    6. Neptune");
        
        System.out.println("");

        System.out.print("Which planet are you visiting? ");
        int planet = scan.nextByte();
        
        System.out.println("");

        if (planet == 1) {
            System.out.println("Your weight would be " + venus + " on that planet");
        } else if (planet == 2) {
            System.out.println("Your weight would be " + mars + " on that planet");
        } else if (planet == 3) {
            System.out.println("Your weight would be " + jupiter + " on that planet");
        } else if (planet == 4) {
            System.out.println("Your weight would be " + saturn + " on that planet");
        } else if (planet == 5) {
            System.out.println("Your weight would be " + uranus + " on that planet");
        } else if (planet == 6) {
            System.out.println("Your weight would be " + neptune + " on that planet");
        }

    }

}
