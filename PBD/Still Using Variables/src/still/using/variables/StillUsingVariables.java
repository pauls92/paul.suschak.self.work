/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package still.using.variables;

/**
 *
 * @author apprentice
 */
public class StillUsingVariables {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name = "Paul Suschak";
        int graduate = 2010;
        
        System.out.println("My name is " + name + " and I graduated in " + graduate);
    }
    
}
