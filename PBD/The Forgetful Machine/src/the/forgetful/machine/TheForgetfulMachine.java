/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package the.forgetful.machine;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TheForgetfulMachine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner forget = new Scanner(System.in);
        System.out.println("Give me a word!");
        String word = forget.nextLine();
        System.out.println("Give me a second word!");
        String second = forget.nextLine();
        System.out.println("Great, now your favorite number?");
        int number = forget.nextInt();
        System.out.println("And your second favorite number?");
        int hat = forget.nextInt();
        System.out.println("Whew, wasn't that fun?");
    }
    
}
