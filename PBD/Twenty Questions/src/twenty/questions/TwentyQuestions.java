/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twenty.questions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TwentyQuestions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner question = new Scanner(System.in);

        String answer = "";
        String answer2 = "";

        System.out.println("Two Questions! ");
        System.out.println("Think of an object, and I'll try to guess it.");

        System.out.println("");

        System.out.print("Is it animal, vegetable, or mineral?");
        answer = question.nextLine();

        System.out.println("Is it bigger than a breadbox?");
        answer2 = question.nextLine();

        if ("animal".equals(answer)) {
            if ("yes".equals(answer2)) {
                System.out.println("Are you thinking of a dog?");
            } else {
                System.out.println("I'm guessing it's a mouse.");
            }
        }
        if ("vegetable".equals(answer)) {
            if ("yes".equals(answer2)) {
                System.out.println("Are you thinking of a pumpkin?");
            } else {
                System.out.println("Is it a carrot?");
            }
        }
        if ("mineral".equals(answer)) {
            if ("yes".equals(answer2)) {
                System.out.println("Are you thinking of a boulder");
            } else {
                System.out.println("Is it a pebble?");
            }
        }
        System.out.println("I would ask if I'm right, but I don't actually care.");
    }

}
