/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package using.variables;

/**
 *
 * @author apprentice
 */
public class UsingVariables {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int value = 113;
        double irrational = 2.71828;
        String science = "Computer Science";
        System.out.println("This is room number " + value);
        System.out.println("e is close to " + irrational);
        System.out.println("I am learning a bit about " + science);
    }
    
}
