/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package variables.and.names;

/**
 *
 * @author apprentice
 */
public class VariablesAndNames {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here    int cars, drivers, passengers, cars_not_driven, cars_driven;
        // this is a comment
        int cars = 100;
        //here is another
        double space_in_a_car = 4;
        // another int type
        int drivers = 30;
        // and another
        int passengers = 90;
        // here is subtraction
        int cars_not_driven = cars - drivers;
        // here is another comment
        int cars_driven = drivers;
        // here is multiplication *
        double carpool_capacity = cars_driven * space_in_a_car;
        // this is division / 
        double average_passengers_per_car = passengers / cars_driven;

        System.out.println("There are " + cars + " cars available.");
        System.out.println("There are only " + drivers + " drivers available.");
        System.out.println("There will be " + cars_not_driven + " empty cars today.");
        System.out.println("We can transport " + carpool_capacity + " people today.");
        System.out.println("We have " + passengers + " to carpool today.");
        System.out.println("We need to put about " + average_passengers_per_car + " in each car.");

        /* Assignment answers:
        1   It isn't necessary as the argument is printed based on the data type in the declaration
        2   A floating point number is one that has no fixed value before or after the decimal point, so the decimal "floats"
        */
    }

}
